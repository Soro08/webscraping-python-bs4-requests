# Apprendre le Web Scraping avec Python, Beautiful Soup et Requests
Dans ce tutoriel de programmation Python, nous allons apprendre à scrapper des sites Web à l'aide de la bibliothèque BeautifulSoup et Requests. BeautifulSoup est un excellent outil qui permet d'analyser le code HTML et d'extraire exactement les informations dont vous avez besoin. Ainsi, que vous récupériez les titres des sites d'information, les scores des sites sportifs ou les prix d'un magasin en ligne... BeautifulSoup et Python vous aideront à le faire rapidement et facilement. Commençons...

## Site exemple:

http://quotes.toscrape.com/

## Virtual environment
Create the virtual environment. In console:

`$ python3 -m venv env`

## Activate the virtual environment:

`$ source env/bin/activate` 

## Installer requests et beautifulsoup:

`$ pip install requests`

`$ pip install beautifulsoup4`
