import requests
from bs4 import BeautifulSoup
import json
import csv



datas = []

page = 1
next_page = True
while next_page:
    url = f"http://quotes.toscrape.com/page/{page}/"
    print(url)
    response = requests.get(url)
    if response.status_code == 200:
        html_doc = response.text
        soup = BeautifulSoup(html_doc, 'html.parser')
        li_next = soup.find('li', {"class":"next"})
        print(page)
        
        html_doc = response.text
        soup = BeautifulSoup(html_doc, 'html.parser')
        
        quotes = soup.findAll('div', {'class':"quote"})
        
        for quote in quotes:
            data = {}
            text = quote.find('span', {'class':"text"})   
            author = quote.find('small', {'class':"author"})
            tags = quote.findAll('a', {"class": "tag"})
            tag_value = ""
            for tag in tags:
                tag_value += " " + tag.text

            data['text'] = text.text
            data['author'] = author.text
            data['tags'] = tag_value
            datas.append(data)
        
    else:
        print("scraping fail")
    
    if li_next:
        page += 1
    else:
        next_page = False


if datas:
    with open("quotes.csv", "w", newline='') as csvfile:
        fieldnames = ['text',"author", "tags"]
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        for data in datas:
            writer.writerow(data)

